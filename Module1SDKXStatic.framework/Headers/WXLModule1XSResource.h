//
//  Module1XDResource.h
//  WXLModule1SDK
//
//  Created by wxiang on 2020/9/23.
//  Copyright © 2020 wxiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WXLModule1XSResource : NSObject
/**
 获取库中图片
 */
+ (UIImage *)imageNamed:(NSString *)name;

/**
 获取库中数据路径
 */
+ (nullable NSString *)pathForResource:(nullable NSString *)name ofType:(nullable NSString *)ext;

/**
 获取当前库的Bundle
 */
+ (NSBundle *)SDKBundle;

/**
 获取当前库的资源文件的Bundle
 */
+ (NSBundle *)resourceBundle;

+ (NSBundle *)staticResourceBundle;
@end

NS_ASSUME_NONNULL_END
