Pod::Spec.new do |spec|

  spec.name         = "Module1SDKXStatic" # 私有库名称
  spec.version      = "1.1.0" # 私有库版本号
  spec.summary      = "xc framework" # 项目简介
  spec.description  = "这是ProjDemo1的,xcode打包的静态库的framework，手动引入的基础组件" # 项目简介

  spec.homepage     = "https://gitee.com/snilwx/static-module1-sdk" # 仓库的主页

  spec.license      = "MIT" # 开源许可证

  spec.author       = { "wangxiang" => "934416194@qq.com" } # 作者信息
  spec.platform     = :ios, "10.0" # 平台及支持的最低版本

  spec.source       = { :git => "https://gitee.com/snilwx/static-module1-sdk.git", :tag => "#{spec.version}" } #仓库地址
  
  spec.dependency 'AFNetworking', '~> 3.0'
  spec.dependency 'BaseSDKXStatic', '~>1.0.4'
  
  #不会自动创建PodsLibDemo.bundle存放打包资源
  spec.resources = 'Module1SDKXStatic.framework/*.{png,xib,nib,bundle}' # 导入资源文件
  #spec.resource_bundles = {
  #  'Module1SDKXStatic' => ['Module1SDKXStatic.framework/*.{png,xib,nib,bundle}']
  #}
  
  #此处为需要添加的 framework
  spec.vendored_framework = 'Module1SDKXStatic.framework'
  #spec.vendored_frameworks = '*.framework' # 指定 .framework 文件PodsLibDemo.framework

  spec.requires_arc = true #是否支持ARC

end
